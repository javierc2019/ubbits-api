<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;


class UbitsController extends Controller
{
   public function get_token(Request $request)
   {
      #return response()->json("Hello world", 201);
      # http://localhost:8000/api/auth_ubits?&uuid=1234567&username=usertest21

      $result = $this->c_encrypt($request->uuid, $request->username);
      return response()->json($result, 201);
      # return response()->json(["uuid" => $request->uuid, "username" => $request->username], 201);
   }

   /**
    * Este método genera el Token necesario para la autenticación con UBits.
    * @param string $uuid Identificador de empresa.
    * @param string $username Nombre de usuario en la plataforma Ubits.
    * @param int $courseId Id del curso al que se quiere entrar.
    * @return string Token con la información necesaria para autenticar.
    * @throws Exception En caso de no encontrar las llaves.
   */
   function c_encrypt(string $uuid, string $username): string {
      $privateKey = $this->c_get_file_content(app_path() . '/Http/controllers/keys/private_key.key');       // path to private key.
      $publicKey = $this->c_get_file_content(app_path() . '/Http/Controllers/keys/u_public_key.key');   // path to ubits public key.
      $message_nonce = $this->c_get_file_content(app_path() . '/Http/Controllers/keys/message_nonce.key'); // path to message_nonce.

      $text = "$uuid|$username|" . $this->c_get_current_time_from_server();
  
      if (!$publicKey)
         return response()->json(["message" => "La llave pública no puede ser nula."], 400);
  
      if (!$privateKey)
         return response()->json(["message" => "La llave privada no puede ser nula."], 400);
  
      if (!$message_nonce)
         return response()->json(["message" => "El mensage_nonce no puede ser nulo."], 400);
  
      $kp = sodium_crypto_box_keypair_from_secretkey_and_publickey(
         $privateKey, $publicKey
      );
  
      $ciphertext = sodium_crypto_box(
         $text, $message_nonce, $kp
      );
  
      $result = rawurlencode(base64_encode($ciphertext));
  
      return $result;
  }  

   /**
    * Este método retorna el contenido del archivo de la ruta indicada.
    * @param string $path Ruta del archivo.
    * @return string Contenido del archivo.
    */
   function c_get_file_content(string $path): string {
      $myFile = $path;
      $myFileLink = fopen($myFile, 'r');
      $myFileContents = fread($myFileLink, filesize($myFile));
      fclose($myFileLink);
      return $myFileContents;
   }

   /**
    * Este método consulta la hora actual a un servidor especifico.
    * @param string $timeserver 
    * @param string $socket
    * @return string
   */
   function c_query_time_server($timeserver, $socket) {
      /* Query a time server
        (C) 1999-09-29, Ralf D. Kloth (QRQ.software) <ralf at qrq.de> */
  
      $fp = fsockopen($timeserver, $socket, $err, $errstr, 5);
      # parameters: server, socket, error code, error text, timeout
      if ($fp) {
          fputs($fp, "\n");
          $timevalue = fread($fp, 49);
          fclose($fp); # close the connection
      } else {
          $timevalue = " ";
      }
  
      $ret = array();
      $ret[] = $timevalue;
      $ret[] = $err;     # error code
      $ret[] = $errstr;  # error text
      return($ret);
  }

   /**
    * Este método retorna la hora actual.
    * @return Hora actual en formato (yyyy-MM-dd HH:mm:ss). 
    */
   function c_get_current_time_from_server() {
      $timeserver = "time-C.timefreq.bldrdoc.gov";
      $timercvd = $this->c_query_time_server($timeserver, 13);
      if (!$timercvd[1]) { # if no error from query_time_server
         $str_time = explode(" ", $timercvd[0]);
         $date = $str_time[1];
         $time = $str_time[2];

         $current_time = "$date $time";
      } #if (!$timercvd)
      else {
         #$current_time = date_format(date(), 'Y-m-d H:i:s');
         $current_time = "la hora";
      } # else

      return $current_time;
   }
}

